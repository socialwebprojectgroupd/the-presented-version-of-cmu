-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 16, 2016 at 08:51 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cmu_plus`
--

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `id_posts` int(11) NOT NULL,
  `author` varchar(255) NOT NULL,
  `comment` text NOT NULL,
  `datetime` datetime NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`id_posts`, `author`, `comment`, `datetime`, `id`) VALUES
(9, 'adminn', '8dgdskgop', '2016-12-16 01:24:53', 1),
(7, 'adminn', 'dddd', '2016-12-16 01:25:03', 2),
(7, 'adminn', 'ddd', '2016-12-16 03:38:21', 3),
(3, 'adminn', 'à¸„à¸§à¸¢', '2016-12-16 03:38:57', 4),
(7, 'adminn', 'dddddd', '2016-12-16 03:41:10', 5),
(2, 'adminn', 'qweqwe', '2016-12-16 06:51:16', 8),
(7, 'adminn', 'ddddds', '2016-12-16 04:48:54', 7),
(2, 'test', 'hello\r\n', '2016-12-16 08:31:36', 9);

-- --------------------------------------------------------

--
-- Table structure for table `friends`
--

CREATE TABLE `friends` (
  `users_add` int(11) NOT NULL,
  `users_admit` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `friends`
--

INSERT INTO `friends` (`users_add`, `users_admit`, `id`, `status`) VALUES
(2, 4, 58, 1),
(5, 4, 55, 2),
(8, 5, 59, 2);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `Section` int(11) NOT NULL,
  `Name_Sec` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `Section`, `Name_Sec`) VALUES
(15, 1225, 'ENGL IN SCIENCE AND TECH CONT'),
(16, 207109, ''),
(17, 259103, 'ENGINEERING MATERIALS'),
(18, 261342, 'FUND OF DATABASE SYSTEMS'),
(19, 269210, 'COM ARCH FOR ISNE'),
(20, 261433, 'NETWORK PROGRAMMING'),
(21, 261343, 'DATABASE SYSTEM LABORATORY'),
(22, 851100, 'INTRO TO COMMUNICATION'),
(23, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `groups_post`
--

CREATE TABLE `groups_post` (
  `id` int(11) NOT NULL,
  `users` text NOT NULL,
  `content` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `sec` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groups_post`
--

INSERT INTO `groups_post` (`id`, `users`, `content`, `user_id`, `sec`) VALUES
(1, 'adminn', 'qqqq', 4, '1225'),
(2, 'phonecyber', 'hello, next term i will get A', 8, '259103');

-- --------------------------------------------------------

--
-- Table structure for table `group_comment`
--

CREATE TABLE `group_comment` (
  `id` int(11) NOT NULL,
  `id_comment` int(11) NOT NULL,
  `author` text NOT NULL,
  `comment` text NOT NULL,
  `datetime` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `group_comment`
--

INSERT INTO `group_comment` (`id`, `id_comment`, `author`, `comment`, `datetime`) VALUES
(1, 3, 'admin', 'dfsdf', '0000-00-00 00:00:00'),
(2, 3, 'admin', 'dfsdf', '0000-00-00 00:00:00'),
(3, 3, 'admin', 'dsfsdf', '0000-00-00 00:00:00'),
(4, 3, 'admin', 'dsfsdf', '0000-00-00 00:00:00'),
(5, 3, 'admin', 'efsf', '0000-00-00 00:00:00'),
(6, 3, 'admin', 'sdfdsf', '0000-00-00 00:00:00'),
(7, 4, 'admin', 'à¸«à¸à¸«à¸”', '0000-00-00 00:00:00'),
(8, 3, 'admin', 'sdf', '0000-00-00 00:00:00'),
(9, 7, 'adminn', 'à¹€à¸«à¸µà¹‰à¸¢', '0000-00-00 00:00:00'),
(10, 4, 'adminn', 'heello', '0000-00-00 00:00:00'),
(11, 1, 'adminn', 'qwe', '0000-00-00 00:00:00'),
(12, 2, 'phonecyber', 'we will make it!', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE `likes` (
  `id` int(11) NOT NULL,
  `id_posts` int(11) NOT NULL,
  `author` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `likes`
--

INSERT INTO `likes` (`id`, `id_posts`, `author`) VALUES
(7, 7, 'testregister'),
(8, 7, 'testregister'),
(9, 7, 'testregister'),
(13, 17, 'admin'),
(17, 6, 'adminn'),
(29, 8, 'adminn'),
(30, 7, 'adminn'),
(31, 9, 'adminn');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `user_id` int(11) NOT NULL,
  `time` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `login_attempts`
--

INSERT INTO `login_attempts` (`user_id`, `time`) VALUES
(1, '1481532442'),
(1, '1481626408'),
(1, '1481626422'),
(1, '1481626428'),
(1, '1481626598'),
(1, '1481740266'),
(1, '1481782938'),
(4, '1481868420');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `msg` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` char(128) NOT NULL,
  `salt` char(128) NOT NULL,
  `student_id` int(9) NOT NULL,
  `pictures` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `username`, `email`, `password`, `salt`, `student_id`, `pictures`) VALUES
(1, 'admin', 'admin@social.com', '55916bc278cd2c6667b211010eac955eb8420d425a196c98991bae80010a716a8025441ff32313bcd31e52c6da20c918677b1b46bb2db1a1ccde075a707853d9', 'f4d9001291e707619cc6d4bf58f5238bd2a88466469911b5aa38435a41d81fe60f3e45b36fc1e97c964487153457a630b6c858837282c9ad71b5f22650436a89', 580611065, 'streets1481808833.jpg'),
(2, 'test', 'test@example.com', 'f08fd3389edac77359bd8af01e8715c53a6606e16d954b05a5151c18a838ae7c86dac8516f7104263e0cd4014488af0d5b047b2d77c0f25033154f78eb8bc51b', 'b457d89fba6a8e51ab35a4e8a4711fb02b1388a7fe3894aaf2cdc37e011df584e6d34180fba8ebbc51ecc22994075a37ec88b3b64d134c16e4be9bfd7e23b82e', 580611051, ''),
(3, 'test1', 'test1@example.com', '2da502d9a21838913244a154e79a591210ec9aa6d4c1c8bd98abdb2d76a4a0c11d600f2aa126a4d83856d4dd896326a36a70281185666d66cc3c92cb4f896f60', '038c34e708f48112b43ba73f59bfad3292ad4f61f44c20e1334948a608be348c68584d5d519cc8ab3c22addfa785e1b49808fe1a275a9399f8b12e8e645a9ff6', 580611003, ''),
(4, 'adminn', 'adminn@social.com', 'e7d666d279f9c508da70e478231cf5baa96a81d9f41dfa80e0a1c0e8f15ab53f0af4e6b2fb0369bc90dc8a3ef74cd503499e6276066a611fc732ceb4d4f8892e', '8fe716332809744f963834633d04ca6a57c4f17a5b9dc2e9e1f420af5e0572dd12ba1b049751ae32f076258d681e07d5793fb23f28d732823a0f508148fdcc68', 580611021, '7317461481867692.png'),
(5, 'papannakub', 'guyp32007@hotmail.com', '7b21f06d7d0b2b3b43d9d19ce8bf8e76fefb2b3c0e6fd555f6bc45071707eb870b49cdefd6674e09f808996c2b7f9070a600fa734f09a9d34bd7e114d0f4868f', 'd0c1cd2dd69ddb513dcb4492fa2d58f93b0e0b8effadf87a8dc47f4b97ac6de413bece30be4f4432c9dbb628ac4ec1c59495a6e797f33285a6aecaeca2ad63a2', 580611042, NULL),
(6, 'papannakub2', 'guyp32008@hotmail.com', 'ac2e6251a6f4fcf88234e458954ea0c08e2be76eec2bf7ee3d632fe5f4807c7394a4e095f0bfc1985fbc11b01584a6168a9fe003dfdad6b39b3b2a95abcc9a9f', 'e0600912570db25e3a145cbea36e4619e16bd3def8c898b66c1a474fe579ddf19ca43e24cf3ca37deef753d105594e5f2e0315c5fe163812b9f1af62fe54d8a6', 580611042, NULL),
(7, 'hello', 'gg@gg.com', 'af659ec6cca03d7f5ce8643a567b4f1340fbfc476ca1fc25d2de0f1dd166f51de7fedb11f7651f03a73f99d9b28a619f281ac3f5b656003ff4bb195f46740036', '6fa4a5b9063d572a6a89c220e97aa110277e4c4769f32a22efb90552a74a5983440c23f5052c366ff7777beee7eee4892711773f3692e2aacb6b92a8fcb4bf74', 580611065, NULL),
(8, 'phonecyber', 'phone@gmail.com', 'b697ecedb9d52b0958b980de95dd485d2a92ed5e239d0306df5440f012b828ba10d84dbaaf10fbe69fda98e22c2e2ed9ea2083464f607ec59c5b8437d9313e61', '48d46f00a947ae6beb442e338120fa53e72bc5f85743d35dd9f456121ab4f5fe9e12800d64fac9fccce50e5fa53d0e7c1095a6dd78e4831ce735bd3ff6d07314', 580611021, '01481873280.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `author` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `id_posts` int(11) NOT NULL,
  `datetime` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `author`, `content`, `id_posts`, `datetime`) VALUES
(1, 'adminn', '<p>qwewqe</p>', 4, '2016-12-16 12:51:08'),
(2, 'adminn', '<p>qwewqe</p>', 4, '2016-12-16 12:51:10'),
(3, 'test', 'qweqweqw', 2, '2016-12-16 09:00:00'),
(4, 'adminn', '<p>asdas</p>', 4, '2016-12-16 13:19:13');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `friends`
--
ALTER TABLE `friends`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups_post`
--
ALTER TABLE `groups_post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group_comment`
--
ALTER TABLE `group_comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `friends`
--
ALTER TABLE `friends`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `groups_post`
--
ALTER TABLE `groups_post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `group_comment`
--
ALTER TABLE `group_comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `likes`
--
ALTER TABLE `likes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
