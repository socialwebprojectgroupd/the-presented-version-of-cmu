<?php date_default_timezone_set("Asia/Bangkok");
  class Post {
    // we define 3 attributes
    // they are public so that we can access them using $post->author directly
    public $id;
    public $author;
    public $content;
    public $id_users;
    public $datetime;

    public function __construct($id, $author, $content,$id_users,$datetime) {
      $this->id      = $id;
      $this->author  = $author;
      $this->content = $content;
      $this->id_users = $id_users;
      $this->datetime = $datetime;
    }

    public static function post($id_user,$name,$content)
  {
    $datetime = date("Y-m-d H:i:s");
    $db = new mysqli("localhost","root","","cmu_plus");
    $req = $db->query("INSERT INTO `posts` (`author`,`content`,`id_posts`,`datetime`) VALUES ('$name','$content','$id_user','$datetime')");
    //print_r($req);
    if($req == true)
    {
    return true;
    }else
    {
      return false;
    }
  }

      public static function edit($id,$content){

        $db = Db::getInstance();
        $datetime = date("Y-m-d H:i:s");
        $qry="UPDATE posts SET content='$content',dateatime='$datetime' WHERE id=$id";

        $result=$db->prepare($qry);

        $add = $result->execute(array(
                                        ":content"=>$content)
                                      );
      }

      public static function delete($id){
      $db = Db::getInstance();
      $qry="DELETE FROM posts WHERE id='$id'";
      $result=$db->query($qry);
    }

    public static function all() {
      $list = [];
      $db = Db::getInstance();
      $req = $db->query('SELECT * FROM posts');

      // we create a list of Post objects from the database results
      foreach($req->fetchAll() as $post) {
        $list[] = new Post($post['id'], $post['author'], $post['content'],$post['id_posts'],$post['datetime']);
      }

      return $list;
    }

      public static function find($id) {
        $db = Db::getInstance();
        // we make sure $id is an integer
        $id = intval($id);
        $req = $db->prepare('SELECT * FROM posts WHERE id = :id');
        // the query was prepared, now we replace :id with our actual $id value
        $req->execute(array('id' => $id));
        $post = $req->fetch();

        return new Post($post['id'], $post['author'], $post['content'],$post['id_posts'],$post['datetime']);
      }
      public static function like($author,$id) {
        $db = Db::getInstance();
        $id = intval($id);
        $qry="INSERT INTO likes (id_posts,author) VALUES (:id,:author)";
      $result=$db->prepare($qry);
      $like = $result->execute(array(  ":id"=>$id,
                                      ":author"=>$author
                                      )
                                    );
      }
      public static function dislike($author,$id) {
        $db = Db::getInstance();
        $id = intval($id);
        $qry="DELETE FROM likes  WHERE id_posts='$id' AND author='$author'";
      $result=$db->query($qry);

      }
      public static function checklike($author,$id){

        $db = Db::getInstance();
        $req = $db->query("SELECT * FROM likes  WHERE id_posts='$id' AND author='$author'");
        $count = $req->rowCount();
        if($count!='1'){
          return true;
        }
        else{
          return false;
        }
      }
      public static function countlike($id){
        $db = Db::getInstance();
        $req = $db->query("SELECT * FROM likes  WHERE id_posts='$id'");
        $count = $req->rowCount();
        return $count;
      }
    }
?>