<?php 
	class Comments{
		  public $id;
      public $id_posts;
      public $author;
      public $comment;
      public $datetime;
      public function __construct($id,$id_posts, $author, $comment,$datetime) {
        $this->id      = $id;
        $this->author  = $author;
        $this->comment = $comment;
        $this->id_posts = $id_posts;
        $this->datetime = $datetime;
      }

    	public static function add($id_post,$author,$comment){
          $db = Db::getInstance();
          $datetime = date("Y-m-d H:i:s");
          $req = $db->query('SELECT * FROM comment');
          $sql = $db->prepare("INSERT INTO `comment` (`id_posts`, `author`, `comment`,`datetime`) VALUES ('$id_post', '$author', '$comment','$datetime');");
           $sql->execute();
           return $sql;
      }

      public static function edit($id,$comment){

        $db = Db::getInstance();
        $qry="UPDATE comment SET comment='$comment' WHERE id=$id";

        $result=$db->prepare($qry);

        $add = $result->execute(array(
                                        ":comment"=>$comment)
                                      );
      }

      public static function delete($id){
      $db = Db::getInstance();
      $qry="DELETE FROM comment WHERE id='$id'";
      $result=$db->query($qry);
      }
      public static function all()
      {
        $list = [];
        $db = Db::getInstance();
        $req = $db->query('SELECT * FROM comment');
        // we create a list of comment objects from the database results
        foreach($req->fetchAll() as $post) {
          $list[] = new Comments($post['id'],$post['id_posts'], $post['author'], $post['comment'], $post['datetime']);
        }
        return $list;
      }

      public static function group_show()
      {
        $list = [];
        $db = Db::getInstance();
        $req = $db->query('SELECT * FROM group_comment');
        // we create a list of comment objects from the database results
        foreach($req->fetchAll() as $post) {
          $list[] = new Comments($post['id'],$post['id_comment'], $post['author'], $post['comment'],$post['datetime']);
        }
        return $list;
      }

      public static function group($id_post,$author,$comment){
          $db = Db::getInstance();
          $datetime = date("Y-m-d H:i:s");
          //$req = $db->query('SELECT * FROM comment');
          $sql = $db->prepare("INSERT INTO `group_comment` (`id_comment`, `author`, `comment`) VALUES ('$id_post', '$author', '$comment');"); 
          $sql->execute();
          return $sql;
      }
	}

?>
