
<?php
  class Group {
    // we define 3 attributes
    // they are public so that we can access them using $post->author directly
    public $id;
    public $content;
    public $sec;
    public $name;
    public $id_posts;

    public function __construct($id, $sec, $name, $content ,$id_posts) {
      $this->id = $id;
      $this->sec  = $sec;
      $this->name = $name;
      $this->content = $content;
      $this->id_posts = $id_posts;
    }
    public static function add($sec,$subject){
      $db = Db::getInstance();
      $id = intval($sec);
      $sql = $db->prepare("INSERT INTO `groups` (`Section`, `Name_Sec`) VALUES ('".addslashes($sec)."','".addslashes($subject)."');"); 
      $sql->execute();
      $id = intval($sec); 
      $req = $db->prepare('SELECT * FROM groups WHERE Section = :id');
      $req->execute(array('id' => $sec));
      $post = $req->fetch();
      return new Group($post['id'], $post['Section'], $post['Name_Sec']);
    }
    public static function into($sec,$subject) {
      $db = Db::getInstance();
      $id = intval($sec);
      $req = $db->prepare('SELECT * FROM groups WHERE Section = :id');
      $req->execute(array('id' => $sec));
      $post = $req->fetch();
      if(!is_array($post)) {
        return Group::add($sec,$subject);
      }else
      {
        return new Group($post['id'], $post['Section'], $post['Name_Sec'],0,0);
      }
    }
    public static function add_post($sec,$name,$content,$id){
        $db = new mysqli("localhost","root","","cmu_plus");
        $req = $db->query("INSERT INTO groups_post (`sec`,`users`,`content`,`user_id`) VALUES ($sec,'".addslashes($name)."','".addslashes($content)."','".addslashes($id)."');");
       return true;
    }
    public static function find($sec){
      $list = [];
       $db = Db::getInstance();
       $req = $db->query('SELECT * FROM groups_post');
       foreach($req->fetchAll() as $group) {
        if($group['sec'] == $sec)
        {
          $list[] = new Group($group['id'],$group['sec'],$group['users'], $group['content'],$group['user_id']);
        }
      }
       return $list;
    }
  }
?>