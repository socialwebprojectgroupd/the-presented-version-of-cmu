<script src='js/tinymce/tinymce.min.js'></script>
		<script>
		          tinymce.init({
		              selector: ".mytextarea",
		              resize: false,
		              plugins: [
		                  "advlist autolink lists link image charmap print preview anchor",
		                  "searchreplace visualblocks code fullscreen",
		                  "insertdatetime media table contextmenu paste textcolor autosave save"
		              ],
		              toolbar: "forecolor backcolor insertfile undo redo  | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent"
		          });
		</script>		
		<!-- Notifications -->
		<div class="container" align="center">
		<h1>Edit Post</h1>
			  <div class="col-md-6 col-md-offset-3 well">
			  <form action="?controller=users&action=update&id_u=<?php echo $_GET['id_u'] ?>" method="post">
			  <input hidden type="text" name="id" value="<?php echo $where->id; ?>">
			  <input class="mytextarea" type="textarea" name="content" value="<?php echo $where->content; ?>">
			  <input type="Submit" class="btn btn-primary btn-sm">
			  </form>
				
			</div>
		
		</div>