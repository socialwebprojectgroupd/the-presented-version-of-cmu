<html>
	<head>
		<title>A Template</title>
		 <script src='js/tinymce/tinymce.min.js'></script>
		<script>
		          tinymce.init({
		              selector: ".mytextarea",
		              resize: false,
		              plugins: [
		                  "advlist autolink lists link image charmap print preview anchor",
		                  "searchreplace visualblocks code fullscreen",
		                  "insertdatetime media table contextmenu paste textcolor autosave save"
		              ],
		              toolbar: "forecolor backcolor insertfile undo redo  | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent"
		          });
		</script>
	</head>

	<body class="gray-bg">

	<?php //var_dump($posts); die(); ?>
		<a class="add-btn" role="button" data-toggle="modal" data-target="#addPost">+</a>
		<!-- Timetable -->
		<div class="container">
			  <div align="center" class="col-md-12 profile-cover">
				<div class="col-md-3 col-md-offset-1 profile-details">
				<div class="profile-pic">

				<?php
					echo '<img src="models/uploads/'.$users->pic.'" style="object-fit: cover;" height="200px" width="200px" class="img-circle">';

				?></div>
				<!-- <?php
					if($_SESSION['user_id'] == $_GET['id_a'])
					{
				?>
				<form action="models/uploads.php?id_u=<?php echo $_GET['id_u'] ?>&id_a=<?php echo $_GET['id_a'] ?>" method="post" enctype="multipart/form-data">
				<input type="file" name="files[]" multiple>
				<input type="submit" value="Upload">
				<?php
					}
				?> -->
<!-- <?php
if($_SESSION['user_id'] == $_GET['id_a']) {
}
else if($is_friends == 1 && $_SESSION['user_id'] != $_GET['id_a'])
{
?>
	<a href="?controller=users&action=delete&id_u=<?php echo $_SESSION['user_id']?>&id_a=<?php echo $_GET['id_a'] ?>">Un Friend</a>
<?php
}else if($is_friends == 2)
{
?>
	<a href="?controller=users&action=delete&id_u=<?php echo $_SESSION['user_id']?>&id_a=<?php echo $_GET['id_a'] ?>">Req Friend</a>
<?php
}
else
{
?>
<a href="?controller=users&action=add&id_u=<?php echo $_SESSION['user_id']?>&id_a=<?php echo $_GET['id_a'] ?>">Add Friend</a>
<?php
}
?> -->
<?php
		$db = new mysqli("localhost","root","","cmu_plus");
		$id = $_GET['id_a'];
    	$req = $db->query("SELECT * FROM `members` WHERE id = '$id'");
    	$name = $req->fetch_object();
?>

				<h1><b><?php echo htmlentities($name->username);?></b></h1>
				</div>
				<div class="col-md-7 biograph">

					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc bibendum dignissim sollicitudin. Duis lobortis vulputate nulla vitae maximus. Nulla id semper nibh. Donec pulvinar neque at felis porttitor euismod. Nullam vel tellus nec dolor rutrum convallis quis uldgasdgadsgadsggadstricies nunc. Nam a accumsan odio. Aliquam finibus mollis congue. Aenean posuere interdum ipsum, id egestas sapien placerat non. Suspendisse dapibus iaculis tristique. Cras ligula neque, auctor ac orci at, efficitur porta nibh. Vivamus posuere iaculis dui. Aliquam id ex convallis, tempus risus nec, malesuada ligula. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec et libero massa.</p><br>
					<p class="fr"><b>9999</b> Friends</p>
				</div>

				<div class="col-md-12" style="text-align: right; margin-top:-15px;margin-bottom: 15px;">
			  	<?php 	
if($_SESSION['user_id'] == $_GET['id_a']) {	
}
else if($is_friends == 1 && $_SESSION['user_id'] != $_GET['id_a'])
{ 
?>
		<a href="?controller=users&action=delete&id_u=<?php echo $_SESSION['user_id']?>&id_a=<?php echo $_GET['id_a'] ?>" class="btn btn-danger" role="button" style="margin-top: 15px;"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span>  Unfriend</a>
<?php
}else if($is_friends == 2)
{	
?>
	<a href="?controller=users&action=delete&id_u=<?php echo $_SESSION['user_id']?>&id_a=<?php echo $_GET['id_a'] ?>" class="btn btn-warning" role="button" style="margin-top: 15px;"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>  Request Sent</a>
<?php
}
else
{	
?>
<a href="?controller=users&action=add&id_u=<?php echo $_SESSION['user_id']?>&id_a=<?php echo $_GET['id_a'] ?>" class="btn btn-success" role="button" style="margin-top: 15px;"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>  Add Friend</a>
<?php
} 
?>
			  	<a href="#" class="btn btn-info" role="button" style="margin-top: 15px;"><span class="glyphicon glyphicon-dashboard" aria-hidden="true"></span>  Edit Profile</a>
			  </div>
			  </div>

		</div>

		<!-- New Feeds -->
		<div class="container">
		  <h1>Posts by <?php echo htmlentities($name->username);?></h1>
		  <div class="row">
		  	<?php


			foreach ($posts as $post) {
				//var_dump($posts);


				if($post->id_users == $_GET['id_a']) {	 ?>
		      <div class="item">
		        <div class="well">
				      <div class="caption">
				        <p><?php echo $post->content; ?></p>
				       	<div style="padding-bottom: 15px;">
				       	 	<p class="right-text float-right">
								<a href="#" class="user-post"><?php echo $post->author ?></a><br>
					       		Posted on <?php echo date('d-m-Y H:i',strtotime($post->datetime));?>
				       		</p>
				       			<?php 
                   if (Post::checklike($_SESSION['username'],$post->id)){?>

                   <a href="?controller=users&action=like&id=<?php echo $post->id;?>&id_u=<?php echo $_GET['id_u'] ?>&id_a=<?php echo $_GET['id_a']; ?>" class="btn btn-primary" role="button" style="margin-top: 15px;"><span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span>   <?php echo Post::countlike($post->id); ?>  Like</a>

                <?php } else{ ?>
                
                <a href="?controller=users&action=dislike&id=<?php echo $post->id;?>&id=<?php echo $post->id;?>&id_u=<?php echo $_GET['id_u'] ?>&id_a=<?php echo $_GET['id_a']; ?>" class="btn btn-default" role="button" style="margin-top: 15px;"><span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span>   <?php echo Post::countlike($post->id); ?>  Liked</a>

                <?php } ?>


									<a href="#" class="btn btn-default" role="button" data-toggle="modal" data-target="#viewPost<?php echo $post->id ?>" style="margin-top: 15px;">View</a>
				        </div>
			      		</div>
		        </div>
		      </div>
				<?php
					}
				}
				?>
		  </div>
		</div>

				<!-- Add - Modal -->
		<div class="modal fade" id="addPost" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h2 class="modal-title" id="myModalLabel">New Post</h2>
		      </div>
		      <div class="modal-body">
		      			<form action="?controller=posts&action=post&id_u=<?php echo $_SESSION['user_id']; ?>" method="post">
						<input hidden type="text" name="id_posts" value="<?php echo $_SESSION['user_id'];?>">
		        <input class="mytextarea" type="textarea" name="contents">
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        <input type="submit" class="btn btn-primary" name="submit">
						</form>
		      </div>
					
		    </div>
		  </div>
		</div>


     <?php
      foreach ($posts as $post) { ?>
        <!-- View - Modal -->
        <div class="modal fade" id="viewPost<?php echo $post->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-body">
                  <div class="right-text">
                <h3 href="#" ><?php echo $post->author; ?></h3>
                  Posted on : Timestamp
              </div>
                    <p><?php echo $post->content; ?></p>      
               <?php 
                   if (Post::checklike($_SESSION['username'],$post->id)){?>

                   <a href="?controller=posts&action=like&id=<?php echo $post->id;?>" class="btn btn-primary" role="button" style="margin-top: 15px;"><span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span>   <?php echo Post::countlike($post->id); ?>  Like</a>

                <?php } else{ ?>
                
                <a href="?controller=posts&action=dislike&id=<?php echo $post->id;?>" class="btn btn-default" role="button" style="margin-top: 15px;"><span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span>   <?php echo Post::countlike($post->id); ?>  Liked</a>

                <?php } ?>

            <?php if($post->id_users == $_SESSION['user_id'])
            {
            ?>
                      <a href="?controller=users&action=edit&id_u=<?php echo $_GET['id_u'] ?>&id=<?php echo $post->id ?>" class="btn btn-default" role="button" style="margin-top: 15px;"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span>  Edit Post</a>
                      <a href="?controller=posts&action=deletepost&id=<?php echo $post->id; ?>&id_u=<?php echo $_GET['id_u'] ?>" class="btn btn-danger" role="button" style="margin-top: 15px;"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span>  Delete Post</a>
            <?php
            }
            ?>
              </div>

              <div class="modal-footer">
              <div class="comments" style="text-align: left;">
              <h3>Comments</h3>
              

             
              <?php foreach ($comment as $comments){
                if(($comments->id_posts == $post->id) == NULL){
                  // echo '<p>This post has no comment.</p>';
                }
                if($comments->id_posts == $post->id)
                { ?>

                <!-- Comment -->
                <div class="media">
                  <a class="pull-left" href="#">
                    <img class="media-object" src="http://placehold.it/64x64" alt="">
                  </a>
                  <div class="media-body">
                      <h4 class="media-heading"><?php echo $comments->author; ?></h4>
                      <p><?php echo $comments->comment; ?></p>
                      <?php if($comments->author==$_SESSION['username']){ ?>
                      <a href='?controller=comments&action=delete&id=<?php echo $comments->id;?>'>Delete comment</a>
                      <?php }?>
                    </div>
                </div>
                

             <?php  } }
             ?>


              <br>
              <h4>Leave a Comment:</h4>
                <form action="?controller=comments&action=insert" method="post">
                  <div class="form-group">
                    <input hidden type="text" name="id_post" value="<?php echo $post->id;?>">

                    <input class="form-control" type="text" name="author" placeholder="author" value="<?php echo htmlentities($_SESSION['username']); ?>" readonly>

                  <textarea class="form-control" rows="2" type="text" placeholder="Your comment here." name="comment"></textarea>
                  </div>
                  <input type="submit" name="submit" class="btn btn-primary">
                </form>
              </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
	</body>
</html>