<body>
<a class="add-btn" role="button" data-toggle="modal" data-target="#addPost">+</a>
<?php
	echo '<p>'.$group->sec.'</p>';
  echo $group->name;
	//var_dump($sec);
	rsort($sec);
?>

<div class="container">
  <h1>Posts in this group</h1>
  </br>
      <div class="row">
      <?php foreach($sec as $post) { ?>
          <div class="item">
            <div class="well">
              <div class="caption">
                <p><?php echo $post->content; ?></p>
                <div style="padding-bottom: 15px;">
                  <p class="right-text float-right">
                    <!--
                    <?php foreach($user as $users)
                    {
                      //echo $users->id;
                      if($post->id_users == $users->id)
                      {
                        echo '<a href="?controller=users&action=profile&id_a='.$users->id.'&id_u='.$_SESSION['user_id'].'">';
                      }
                    }?> -->

                    <?php echo $post->name; ?></a><br>
                    Posted on : Timestamp
                  </p>
                  <button class="btn btn-default" role="button" data-toggle="modal" data-target="#viewPost<?php echo $post->id ?>" style="margin-top: 15px;">View</button>
                </div>      
                </div>
            </div>
          </div>
        <?php } ?>
    </div>
</div>


<!-- Add - Modal -->
    <div class="modal fade" id="addPost" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">


          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h2 class="modal-title" id="myModalLabel">New Post</h2>
          </div>


          <div class="modal-body">
          <form action="?controller=groups&action=add_post&id_u=<?php echo $_SESSION['user_id'] ?>&sec=<?php echo $_GET['sec'] ?>&subject=<?php echo $_GET['subject'] ?>" method="post">
            <input hidden type="text" name="id_posts" value="<?php echo $_SESSION['user_id'] ?>">
            <input class="mytextarea" type="textarea" name="contents">
          </div>


          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <input type="submit" class="btn btn-primary"></form>
          </div>
        </div>
      </div>
    </div>



    <?php
      foreach ($sec as $post) { ?>
        <!-- View - Modal -->
        <div class="modal fade" id="viewPost<?php echo $post->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-body">
                  <div class="right-text">
                <h3 href="#" ><?php echo $post->name; ?></h3>
                  Posted on : Timestamp
              </div>
                    <p><?php echo $post->content; ?></p>      
              </div>
               <!--  <?php if($post->id_posts == $_SESSION['user_id'])
            {
            ?>
                      <a href="?controller=users&action=edit&id_u=<?php echo $_GET['id_u'] ?>&id=<?php echo $post->id ?>" class="btn btn-default" role="button" style="margin-top: 15px;"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span>  Edit Post</a>
                      <a href="?controller=posts&action=deletepost&id=<?php echo $post->id; ?>&id_u=<?php echo $_GET['id_u'] ?>" class="btn btn-danger" role="button" style="margin-top: 15px;"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span>  Delete Post</a>
            <?php
            }
            ?> -->
              <div class="modal-footer">
              <div class="comments" style="text-align: left;">
              <h3>Comments</h3>
                <?php if($comment == NULL){
                  echo '<p>This post has no comment.</p>';
                }
               foreach ($comment as $comments){
                if($comments->id_posts == $post->id)
                { ?>

                <!-- Comment -->
                <div class="media">
                  <a class="pull-left" href="#">
                    <img class="media-object" src="http://placehold.it/64x64" alt="">
                  </a>
                  <div class="media-body">
                      <h4 class="media-heading"><?php echo $comments->author; ?></h4>
                      <p><?php echo $comments->comment; ?></p>
                      <?php if($comments->author==$_SESSION['username']){ ?>
                      <a href='?controller=comments&action=delete&id=<?php echo $comments->id;?>'>Delete comment</a>
                      <?php }?>
                    </div>
                </div>
              

             <?php  } }
             ?>
              <br>
              <h4>Leave a Comment:</h4>
                <form action="?controller=comments&action=group&sec=<?php echo $_GET['sec']?>&subject=<?php echo $_GET['subject'] ?>&id_u=<?php echo $_SESSION['user_id'] ?>" method="post">
                  <div class="form-group">
                    <input hidden type="text" name="id_post" value="<?php echo $post->id;?>">

                    <input class="form-control" type="text" name="author" placeholder="author" value="<?php echo htmlentities($_SESSION['username']); ?>" readonly>
                    
                  <textarea class="form-control" rows="2" type="text" placeholder="Your comment here." name="comment"></textarea>
                  </div>
                  <input type="submit" name="submit" class="btn btn-primary">
                </form>
              </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>