<head>
    <script src='js/tinymce/tinymce.min.js'></script>
    <script>
              tinymce.init({
                  selector: ".mytextarea",
                  resize: false,
                  plugins: [
                      "advlist autolink lists link image charmap print preview anchor",
                      "searchreplace visualblocks code fullscreen",
                      "insertdatetime media table contextmenu paste textcolor autosave save"
                  ],
                  toolbar: "forecolor backcolor insertfile undo redo  | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent"
              });
    </script>
</head>
<body>
<?php require_once('main.php'); 
?>
<a class="add-btn" role="button" data-toggle="modal" data-target="#addPost">+</a>
<br>
<div class="container">
  <h1>News feed</h1>

  </br>
      <div class="row">

  <?php rsort($posts) ?>
  <?php foreach($posts as $post) { 
    ?> 
          <?php foreach ($friend as $key) {
              if($_SESSION['user_id'] == $key->users_add || $key->users_admit == $_SESSION['user_id'] && $key->status == 1)
              {
           ?>
          <div class="item">
            <div class="well">
              <div class="caption">

                <p><?php echo $post->content; ?></p>
                <div style="padding-bottom: 15px;">
                  <p class="right-text float-right">
                    <?php foreach($user as $users)
                    {
                      //echo $users->id;
                      if($post->id_users == $users->id)
                      {
                        echo '<a class="user-post" href="?controller=users&action=profile&id_a='.$users->id.'&id_u='.$_SESSION['user_id'].'">';
                      }
                    }?><?php echo $post->author; ?></a><br>

                    Posted on : <?php echo $post->datetime; ?>
                  </p>

                  <?php 
                   if (Post::checklike($_SESSION['username'],$post->id)){?>

                   <a href="?controller=posts&action=like&id=<?php echo $post->id;?>" class="btn btn-primary" role="button" style="margin-top: 15px;"><span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span>   <?php echo Post::countlike($post->id); ?>  Like</a>

                <?php } else{ ?>
                
                <a href="?controller=posts&action=dislike&id=<?php echo $post->id;?>" class="btn btn-default" role="button" style="margin-top: 15px;"><span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span>   <?php echo Post::countlike($post->id); ?>  Liked</a>

                <?php } ?>


                  
                  <a href="#" class="btn btn-default" role="button" data-toggle="modal" data-target="#viewPost<?php echo $post->id ?>" style="margin-top: 15px;">View</a>
                </div>      
                </div>
            </div>
          </div>
        <?php
        break;
                        }
          }
        }
        ?>
      </div>
    </div>


</div>


    <!-- Add - Modal -->
    <div class="modal fade" id="addPost" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h2 class="modal-title" id="myModalLabel">New Post</h2>
          </div>
          <div class="modal-body">
          <form action="?controller=posts&action=post&id_u=<?php echo $_SESSION['user_id'] ?>" method="post">
            <input hidden type="text" name="id_posts" value="<?php echo $_SESSION['user_id'] ?>">
            <input class="mytextarea" type="textarea" name="contents">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <input type="submit" class="btn btn-primary"></form>
          </div>
        </div>
      </div>
    </div>



    <?php
      foreach ($posts as $post) { ?>
        <!-- View - Modal -->
        <div class="modal fade" id="viewPost<?php echo $post->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-body">
                  <div class="right-text">
                <h3 href="#" ><?php echo $post->author; ?></h3>
                  Posted on : Timestamp
              </div>
                    <p><?php echo $post->content; ?></p>      
               <?php 
                   if (Post::checklike($_SESSION['username'],$post->id)){?>

                   <a href="?controller=posts&action=like&id=<?php echo $post->id;?>" class="btn btn-primary" role="button" style="margin-top: 15px;"><span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span>   <?php echo Post::countlike($post->id); ?>  Like</a>

                <?php } else{ ?>
                
                <a href="?controller=posts&action=dislike&id=<?php echo $post->id;?>" class="btn btn-default" role="button" style="margin-top: 15px;"><span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span>   <?php echo Post::countlike($post->id); ?>  Liked</a>

                <?php } ?>

            <?php if($post->id_users == $_SESSION['user_id'])
            {
            ?>
                      <a href="?controller=users&action=edit&id_u=<?php echo $_GET['id_u'] ?>&id=<?php echo $post->id ?>" class="btn btn-default" role="button" style="margin-top: 15px;"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span>  Edit Post</a>
                      <a href="?controller=posts&action=deletepost&id=<?php echo $post->id; ?>&id_u=<?php echo $_GET['id_u'] ?>" class="btn btn-danger" role="button" style="margin-top: 15px;"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span>  Delete Post</a>
            <?php
            }
            ?>
              </div>

              <div class="modal-footer">
              <div class="comments" style="text-align: left;">
              <h3>Comments</h3>
              

             
              <?php foreach ($comment as $comments){
                if(($comments->id_posts == $post->id) == NULL){
                  // echo '<p>This post has no comment.</p>';
                }
                if($comments->id_posts == $post->id)
                { ?>

                <!-- Comment -->
                <div class="media">
                  <a class="pull-left" href="#">
                    <img class="media-object" src="http://placehold.it/64x64" alt="">
                  </a>
                  <div class="media-body">
                      <h4 class="media-heading"><?php echo $comments->author; ?></h4>
                      <p><?php echo $comments->comment; ?></p>
                      <?php if($comments->author==$_SESSION['username']){ ?>
                      <a href='?controller=comments&action=delete&id=<?php echo $comments->id;?>'>Delete comment</a>
                      <?php }?>
                    </div>
                </div>
                

             <?php  } }
             ?>


              <br>
              <h4>Leave a Comment:</h4>




                <form action="?controller=comments&action=insert" method="post">
                  <div class="form-group">
                    <input hidden type="text" name="id_post" value="<?php echo $post->id;?>">

                    <input class="form-control" type="text" name="author" placeholder="author" value="<?php echo htmlentities($_SESSION['username']); ?>" readonly>
                    
                  <textarea class="form-control" rows="2" type="text" placeholder="Your comment here." name="comment"></textarea>
                  </div>
                  <input type="submit" name="submit" class="btn btn-primary">
                </form>
              </div>
              </div>
            </div>
          </div>
        </div>

      
</body>





  <!--
  <form action="?controller=posts&action=post" method="post">
        <input hidden type="text" name="id_post" value="<?php echo $_SESSION['user_id'];?>">
        <?php echo $_SESSION['username'] ?>
        <input type="text" placeholder="post something " name="post">
        <input type="submit" name="submit">
  </form>
  -->




  <!--

  <div class="posts">
    <p>
      <?php foreach($user as $users)
      {
        //echo $users->id;
        if($post->id_users == $users->id)
        {
          echo '<a href="?controller=users&action=profile&id_a='.$users->id.'&id_u='.$_SESSION['user_id'].'">';
        }
      }
      ?>
      <?php echo $post->author; ?></a>
      </br>
      <?php echo $post->content; ?>
    </p>


  <?php echo Post::countlike($post->id);
     if (Post::checklike($_SESSION['username'],$post->id)){?>
  <a href='?controller=posts&action=like&id=<?php echo $post->id;?>'><button>Like</button></a>

  <?php } else{ ?>
  <a href='?controller=posts&action=dislike&id=<?php echo $post->id;?>'><button>disLike</button></a>
  <?php } ?>
  <?php if($post->author==$_SESSION['username']){ ?>
  <a href='?controller=posts&action=delete&id=<?php echo $post->id;?>'><button>Delete</button></a>



  <button>Edit</button><br> 

  <form action="?controller=posts&action=edit" method="post">
      <input hidden type="text" name="id" value="<?php echo $post->id;?>">
       <?php echo $post->author;?>
    <input type="text" value="<?php echo $post->content;?>" name="content">
    <input type="submit" name="edit" value="Edit">
  </form>
  <?php } ?>



      <p>comment</p>
      <?php
          $count = 0;
          foreach ($comment as $comments){
              if($comments->id_posts == $post->id && $count < 3)
              {
                  echo $comments->author."  ";
                  echo $comments->comment.'</br>';
                  $count++;
                  ?>
                  <?php if($comments->author==$_SESSION['username']){ ?>
                  <a href='?controller=comments&action=delete&id=<?php echo $comments->id;?>'><button>Delete</button></a>
                  <button>Edit</button><br>
                  <form action="?controller=comments&action=edit" method="post">
                      <input hidden type="text" name="id" value="<?php echo $comments->id;?>">
                       <?php echo $comments->author;?>

                    <input type="text" value="<?php echo $comments->comment;?>" name="comment">
                    <input type="submit" name="edit" value="Edit">
                  </form>
        <?php      }
              }
               } ?>
       </br>
       <div> make popup</div>

      <?php
          foreach ($comment as $comments){
              if($comments->id_posts == $post->id)
              {
                echo $comments->author."  ";
                echo $comments->comment.'</br>';
                ?>
                <?php if($comments->author==$_SESSION['username']){ ?>
                <a href='?controller=comments&action=delete&id=<?php echo $comments->id;?>'><button>Delete</button></a>
                <button>Edit</button><br>
                <form action="?controller=comments&action=edit" method="post">
                    <input hidden type="text" name="id" value="<?php echo $comments->id;?>">

                    <?php echo $comments->author;?>
                  <input type="text" value="<?php echo $comments->comment;?>" name="comment">
                  <input type="submit" name="edit" value="Edit">
                </form>
      <?php  }
              }
       } ?>

      <form action="?controller=comments&action=insert" method="post">
          <input hidden type="text" name="id_post" value="<?php echo $post->id;?>">
          <?php echo htmlentities($_SESSION['username']); ?>
        <input type="text" placeholder="comment" name="comment">
        <input type="submit" name="submit">
      </form>
     </div>
  <?php } ?>
-->