    <!-- Navigation -->
      <nav id="custom-bootstrap-menu" class="navbar navbar-default">
      <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">CMU+</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right">


          <form  class="navbar-form navbar-left" action="?controller=users&action=find_uname" method="post">
           <div class="form-group">
            <input type="text" name="username" class="form-control" placeholder="Search">
            </div>
            <input class="btn btn-default" type="submit" name="submit">
          </form>



            <li><a class="white" href='?controller=posts&action=index&id_u=<?php echo $_SESSION['user_id']?>'><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Home</a></li>
            <li><a href="?controller=disc&action=index&id_u=<?php echo $_SESSION['user_id'] ?>"><span class="glyphicon glyphicon-comment" aria-hidden="true"></span> Discuss</a></li>
            
            <li><a href="?controller=noti&action=index&id_u=<?php echo $_SESSION['user_id'] ?>"><span class="glyphicon glyphicon-globe" aria-hidden="true"></span> Notifications</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></a>
              <ul class="dropdown-menu">
                <li><a href="?controller=users&action=profile&id_u=<?php echo $_SESSION['user_id']?>&id_a=<?php echo $_SESSION['user_id'] ?>">View Profile</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="?controller=users&action=editprofile&id_u=<?php echo $_SESSION['user_id']?>&id_a=<?php echo $_SESSION['user_id'] ?>">Settings</a></li>
                <li><a href='includes/logout.php'>Log out</a></li>
              </ul>
            </li>
          </ul>

        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>