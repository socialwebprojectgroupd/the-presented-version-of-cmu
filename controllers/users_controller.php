<?php
  class UsersController {
    public function profile() {
      // we store all the posts in a variable
      $users = Users::find($_GET['id_a']);
      $posts = Post::all();
      $comment = Comments::all();
      $is_friends = Users::is_friend($_GET['id_u'],$_GET['id_a']);
      $alert = Users::alert($_GET['id_u']);
      require_once('views/nav.php');
      require_once('views/profile/profile.php');
    }
    public function add() {
      $users = Users::add($_GET['id_u'],$_GET['id_a']);
      $is_friends = Users::is_friend($_GET['id_u'],$_GET['id_a']);
      $users = Users::find($_GET['id_a']);
      $alert = Users::alert($_GET['id_u']);
       header('Location: ?controller=users&action=profile&id_u='.$_GET['id_u'].'&id_a='.$_GET['id_a'].'');
    }

    public function delete() {
      $users = Users::delete($_GET['id_u'],$_GET['id_a']);
      $is_friends = Users::is_friend($_GET['id_u'],$_GET['id_a']);
      $users = Users::find($_GET['id_a']);
      $alert = Users::alert($_GET['id_u']);
      header('Location: ?controller=users&action=profile&id_u='.$_GET['id_u'].'&id_a='.$_GET['id_a'].'');
    }
     public function deletepost() {
      $delete = Users::deletepost($_GET['id']);
      header('Location: ?controller=users&action=profile&id_u='.$_GET['id_u'].'&id_a='.$_GET['id_a'].'');
    }
    public function pic()
    {
      $confirm = Users::picture($_GET['id_u'],$_GET['id_a']);
      header('Location: ?controller=users&action=profile&id_u='.$_GET['id_u'].'&id_a='.$_GET['id_a'].'');
    }
    public function edit()
    {
      // $edit = Users::edit($_POST['id'],$_POST['contents']);
      $where = Post::find($_GET['id']);
      require_once('views/nav.php');
      require_once('views/edit.php');
    }
    public function find_uname()
    {
      $user = Users::find_uname($_POST['username']);
      header('Location: ?controller=users&action=profile&id_u='.$_SESSION['user_id'].'&id_a='.$user->id.'');
    }
    public function update()
    {
      // $edit = Users::edit($_POST['id'],$_POST['contents']);
      $where = Post::edit($_POST['id'],$_POST['content']);
      header('Location: ?controller=users&action=profile&id_u='.$_GET['id_u'].'&id_a='.$_GET['id_a'].'');
    }
    
    public function editprofile()
    {
    $users = Users::find($_GET['id_a']);
    require_once('views/nav.php');
      require_once('views/profile/editprofile.php');
    }
    public function like() {
     $post = Post::like($_SESSION['username'],$_GET['id']);
   header('Location: ?controller=users&action=profile&id_u='.$_SESSION['user_id'].'&id_a='.$_GET['id_a'].'');
   }
   public function dislike() {
     $post = Post::dislike($_SESSION['username'],$_GET['id']);
   header('Location: ?controller=users&action=profile&id_u='.$_SESSION['user_id'].'&id_a='.$_GET['id_a'].'');
   }
  }

?>