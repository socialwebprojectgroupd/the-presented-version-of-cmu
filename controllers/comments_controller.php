<?php


 class CommentsController{
     public function insert() {
      // we store all the posts in a variable
      $posts = Comments::add($_POST['id_post'],$_SESSION['username'],$_POST['comment']);
      header('Location: ?controller=posts&action=index&id_u='.$_SESSION['user_id'].'');
    }
    public function edit(){
       $post = Comments::edit(/*$_SESSION['username']*/$_POST['id'],$_POST['comment']);
       header ('Location: ?controller=posts&action=index&id_u='.$_SESSION['user_id'].'');
     }
     public function delete(){
       $post = Comments::delete($_GET['id']);
       header ('Location:?controller=posts&action=index&id_u='.$_SESSION['user_id'].'');
     }
     public function group() {
      // we store all the posts in a variable
      $posts = Comments::group($_POST['id_post'],$_POST['author'],$_POST['comment']);
      
      header('Location: ?controller=groups&action=index&sec='.$_GET['sec'].'&subject='.$_GET['subject'].'&id_u='.$_GET['id_u'].'');
    }
     public function show() {
       // we expect a url of form ?controller=posts&action=show&id=x
       // without an id we just redirect to the error page as we need the post id to find it in the database
       if (!isset($_GET['id']))
         return call('pages', 'error');

       // we use the given id to get the right post
     //  $comments = Comments::find($_GET['id']);
       require_once('views/posts/show.php');
     }
   }
?>