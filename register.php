<?php
/**
 * Copyright (C) 2013 peredur.net
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
include_once 'includes/register.inc.php';
include_once 'includes/functions.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Secure Login: Registration Form</title>
         <!-- Bootstrap Core CSS -->
        <link href="styles/bootstrap.min.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="styles/timetable.css" rel="stylesheet">
        <link href="styles/pinterest-grid.css" rel="stylesheet">
        
        <script type="text/JavaScript" src="js/sha512.js"></script>
        <script type="text/JavaScript" src="js/forms.js"></script>
        <link rel="stylesheet" href="styles/main.css" />
    </head>
    <body class="index-body">
        <!-- Registration form to be output if the POST variables are not
        set or if the registration script caused an error. -->
       
        <?php
        if (!empty($error_msg)) {
            echo $error_msg;
        }
        ?>
        
        <div class="container">
         <div class="col-md-3 col-md-offset-4 home-panel reg" style="padding-top: 180px;"> 

         <p>Username must contains only 0-9, A-Z, a-z and _.<br><br><br><br>
         Password must be at least 6 characters long and includes at least one of each 0-9, A-Z and a-z.<br>
         Password and confirmation must be exactly match.</p>
            </div>
            <div class="col-md-4 home-panel reg">
                <h1>CMU+</h1>
                <br>
                <h3>Register</h3>
                <form name="registration_form" method="post" action="<?php echo esc_url($_SERVER['PHP_SELF']); ?>"  onsubmit="return validateForm()">
                    <!-- redirected to next.html to show user -->
                    <input type="text" id="username" name='username' placeholder="Username"><br>
                    <input type="email" id="email" name="email" placeholder="Email" value=""><br>
                    <input type="password" id="pass" name="password" placeholder="Password"><br>
                    <input type="password" id="re_pass" name="confirmpwd" placeholder="Confirm Password"><br>
                    <input type="number" id="stu_id" name='student_id' placeholder="Student ID"><br>
                    <input type="submit" name="login" value="Register"  onclick="return regformhash(this.form,
                                   this.form.username,
                                   this.form.email,
                                   this.form.password,
                                   this.form.confirmpwd);">
                </form>
                <p>Already a user? <a href="login.php">Log in Here</a></p>
            </div>
        </div>
        
        
        
         
     <!-- jQuery -->
        <script src="js/jquery.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
        <!-- Scrolling Nav JavaScript -->
        <script src="js/jquery.easing.min.js"></script>
        <script src="js/scrolling-nav.js"></script>
    </body>
</html>
