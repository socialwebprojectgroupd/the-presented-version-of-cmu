 <!-- Bootstrap Core CSS -->
<link href="styles/bootstrap.min.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="styles/timetable.css" rel="stylesheet">
<link href="styles/pinterest-grid.css" rel="stylesheet">

<?php
include_once 'includes/db_connect.php';
include_once 'includes/functions.php';

sec_session_start();

if (login_check($mysqli) == true) {
    $logged = 'in';
} else {
    $logged = 'out';
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Secure Login: Log In</title>
        <link rel="stylesheet" href="styles/main.css" />
        <script type="text/JavaScript" src="js/sha512.js"></script> 
        <script type="text/JavaScript" src="js/forms.js"></script> 
    </head>
    <body class="index-body">
    
        <?php
        if (isset($_GET['error'])) {
            echo '<p class="error">Error Logging In!</p>';
        }
        ?> 
        <div class="container">
            <div class="col-md-4 col-md-offset-7 home-panel login">
                <h1>CMU+</h1>
                <br>
                <h3>Log in</h3>
                <form name="login_form" method="post" action="includes/process_login.php"  >
                    <!-- redirected to next.html to show user -->
                    <input type="text" id="email" name="email" placeholder="Email"><br>
                    <input type="password" id="password" name="password"  placeholder="Password"><br>
                    <input type="submit" name="login" value="Log in" onclick="formhash(this.form, this.form.password);">
                </form>
                <p>New user? <a href="register.php">Register Here</a></p>
                <p>If you are done, please <a href="includes/logout.php">log out</a>.</p>
                <p>You are currently logged <?php echo $logged ?>.</p>
            </div>
        </div>
        <!--
        <form action="includes/process_login.php" method="post" name="login_form"> 			
            Email: <input type="text" name="email" />
            Password: <input type="password" 
                             name="password" 
                             id="password"/>
            <input type="button" 
                   value="Login" 
                   onclick="formhash(this.form, this.form.password);" /> 
        </form>
        <p>If you don't have a login, please <a href="register.php">register</a></p>
        <p>If you are done, please <a href="includes/logout.php">log out</a>.</p>
        <p>You are currently logged <?php echo $logged ?>.</p>-->
        
        
       <!-- jQuery -->
        <script src="js/jquery.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
        <!-- Scrolling Nav JavaScript -->
        <script src="js/jquery.easing.min.js"></script>
        <script src="js/scrolling-nav.js"></script>
    </body>
</html>
